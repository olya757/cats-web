﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace cats_web.Controllers
{
    public class CatsController : ApiController
    {
        public string Get_GoodResult()
        {
            return "good result";
        }

        public string Get_BadResult(int num)
        {
            throw new HttpResponseException(HttpStatusCode.NotFound);
        }

        public string Get_WaitResult(int num, bool flag)
        {
            Thread.Sleep(2000);
            return "Sorry for waiting";
        }
    }
}
